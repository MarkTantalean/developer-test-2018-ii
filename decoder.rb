# primeros 2 bytes :
# 00 = login      ,
# 01 = posicion
# 02 = evento
#
########### caso login:
# respuesta : 100
#
########### caso posicion:
# respuesta: si es valido=111, si es invalido=110
# dato | bytes
# id = 2-9
# datetime = 10-19
# sur o norte = 20 (SUR vuelve negativo la coordenada)
# lat = 21-27
# oeste o este = 28 (OESTE vuelve negativo la coordenada)
# lon = 29-35
# validez = 36 (A=valido,V=invalido)
# velocidad = 37-38
#
#
#
########### caso evento
#respusta : 120
# dato | bytes
# output_code = 2-3
# output_value = 4-5
require 'active_support/all'

class Decoder

	@id
	@datetime
	@speed
	@valid
	@lat
	@lon
	@response
	@pack
	@output_code
	@output_value

	attr_accessor :id, :datetime, :speed, :valid, :lat, :lon, :response,
					:output_code, :output_value

	def initialize

	end

	def read(pack)
		@pack = pack
		self
	end

	def login
		if @pack == "00"
			@response = "100"
			return true
		end
		return false
	end

	def posicion
		if @pack[0,2] == "01"
			decoderPosicion()
			if @valid == "A"
				@response = "111"
			else
				@response = "110"
			end
			return true
		end
		return false
	end

	def decoderPosicion()
		@id = @pack[2..9]
		@datetime = Time.at(@pack[10..19].to_i).strftime("%Y-%m-%d %H:%M:%S")
		puntoCardinal = @pack[20]
		if puntoCardinal == "S"
			@lat = @pack[21..27].to_f * -1
		else
			@lat = @pack[21..27].to_f
		end

		puntoCardinal = @pack[28]
		if puntoCardinal == "W"
			@lon = @pack[29..35].to_f * -1
		else
			@lon = @pack[29..35].to_f
		end

		@valid = @pack[36]
		@speed = @pack[37..38]
	end

	def evento
		if @pack[0,2] == "02"
			decoderEvento()
			@response = 120
			return true
		end
		return false
	end

	def decoderEvento()
		@output_code = @pack[2..3]
		@output_value = @pack[4..5]
	end

end